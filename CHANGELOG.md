﻿# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Planed]
### Added
- Command line options
  - initialize
  - open
  - update
  - version
  - help
- Graphic command view
- Repository tree
- File tree
- History graph
- File content view/editor
- Work space view
- Command console
- Context menu
- Logging functionality
- Multi-language support
- Cross platform
- Continuous integration
- Git Flow
- Custom scripts for custom commands
- Unit tests
- Save opened repositories within sqlite data base
- Customize default window layout
- Custom window layout by repository
- Documentation

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [InDevelopment]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [Unreleased]
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [0.0.0] - ????-??-??
### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security
